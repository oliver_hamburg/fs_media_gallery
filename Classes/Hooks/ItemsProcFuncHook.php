<?php
namespace MiniFranske\FsMediaGallery\Hooks;

/*                                                                        *
 * This script is part of the TYPO3 project.                              *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use MiniFranske\FsMediaGallery\Utility\TemplateLayout;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * ItemsProcFuncHook
 */
class ItemsProcFuncHook
{


    /** @var TemplateLayout $templateLayoutsUtility */
    protected $templateLayoutsUtility;

    public function __construct()
    {
        $this->templateLayoutsUtility = GeneralUtility::makeInstance(TemplateLayout::class);
    }

    /**
     * Sets the available actions for settings.switchableControllerActions
     *
     * @param array &$config
     * @return void
     */
    public function getItemsForSwitchableControllerActions(array &$config)
    {
        $availableActions = [
            'nestedList' => 'MediaAlbum->nestedList;MediaAlbum->showAsset',
            'flatList' => 'MediaAlbum->flatList;MediaAlbum->showAlbum;MediaAlbum->showAsset',
            'showAlbumByParam' => 'MediaAlbum->showAlbum;MediaAlbum->showAsset',
            'showAlbumByConfig' => 'MediaAlbum->showAlbumByConfig;MediaAlbum->showAsset',
            'randomAsset' => 'MediaAlbum->randomAsset',
        ];
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fs_media_gallery']);
        $allowedActions = [
            // index action is always allowed
            // this is needed to make sure the correct tabs/fields are shown in
            // flexform when a new plugin is added
            'index' => 'MediaAlbum->index',
        ];
        $allowedActionsFromExtConf = [];
        if (!empty($extConf['allowedActionsInFlexforms'])) {
            $allowedActionsFromExtConf = GeneralUtility::trimExplode(',', $extConf['allowedActionsInFlexforms']);
        }
        foreach ($allowedActionsFromExtConf as $allowedActionFromExtConf) {
            if (!empty($availableActions[$allowedActionFromExtConf])) {
                $allowedActions[$allowedActionFromExtConf] = $availableActions[$allowedActionFromExtConf];
            }
        }
        // check items; allow all actions if something went wrong (no action except of "indexAction" is allowed)
        if (count($allowedActions) > 1) {
            foreach ($config['items'] as $key => $item) {
                if (!in_array($item[1], $allowedActions)) {
                    unset($config['items'][$key]);
                }
            }
        }
    }

    /**
     * Sets the available options for settings.list.orderBy
     *
     * @param array &$config
     * @return void
     */
    public function getItemsForListOrderBy(array &$config)
    {
        $availableOptions = ['datetime', 'crdate', 'sorting'];
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fs_media_gallery']);
        $allowedOptions = [];
        $allowedOptionsFromExtConf = [];
        if (!empty($extConf['list.']['orderOptions'])) {
            $allowedOptionsFromExtConf = GeneralUtility::trimExplode(',', $extConf['list.']['orderOptions']);
        }
        foreach ($allowedOptionsFromExtConf as $allowedOptionFromExtConf) {
            if (in_array($allowedOptionFromExtConf, $availableOptions)) {
                $allowedOptions[] = $allowedOptionFromExtConf;
            }
        }
        foreach ($config['items'] as $key => $item) {
            // check items; empty value (inherit from TS) is always allowed
            if (!empty($item[1]) && !in_array($item[1], $allowedOptions)) {
                unset($config['items'][$key]);
            }
        }
    }

    /**
     * Sets the available options for settings.album.assets.orderBy
     *
     * @param array &$config
     * @return void
     */
    public function getItemsForAssetsOrderBy(array &$config)
    {
        // default set
        $allowedOptions = ['name', 'crdate', 'title', 'content_creation_date', 'content_modification_date'];
        $availableOptions = [];
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['fs_media_gallery']);

        if (!empty($extConf['asset.']['orderOptions'])) {
            $allowedOptions = GeneralUtility::trimExplode(',', $extConf['asset.']['orderOptions']);
        }
        // check if field exists in TCA of sys_file or sys_file_metadata
        foreach ($allowedOptions as $key => $option) {
            if (
                $option === 'crdate'
                ||
                !empty($GLOBALS['TCA']['sys_file']['columns'][$option])
                ||
                !empty($GLOBALS['TCA']['sys_file_metadata']['columns'][$option])
            ) {
                $availableOptions[] = $option;
            }
        }
        // @todo: add option to add custom options to the item list
        //        use label from TCA
        foreach ($config['items'] as $key => $item) {
            // check items; empty value (inherit from TS) is always allowed
            if (!empty($item[1]) && !in_array($item[1], $availableOptions)) {
                unset($config['items'][$key]);
            }
        }
    }


    /**
     * Itemsproc function to extend the selection of templateLayouts in the plugin
     *
     * Inspired by Tx_News_Hooks_ItemsProcFunc
     *
     * @param array &$config configuration array
     */
    public function user_templateLayout(array &$config)
    {
        $pageId = 0;
        if (ExtensionManagementUtility::isLoaded('compatibility6')) {
            if (StringUtility::beginsWith($config['row']['uid'], 'NEW')) {
                $getVars = GeneralUtility::_GET('edit');
                if (is_array($getVars) && isset($getVars['tt_content']) && is_array($getVars['tt_content'])) {
                    $keys = array_keys($getVars['tt_content']);
                    $firstKey = (int)$keys[0];
                    if ($firstKey > 0) {
                        $pageId = $firstKey;
                    } else {
                        $row = $this->getContentElementRow(abs($firstKey));
                        $pageId = $row['pid'];
                    }
                }
            } else {
                $row = $this->getContentElementRow($config['row']['uid']);
                $pageId = $row['pid'];
            }
        } else {
            $pageId = $this->getPageId($config['flexParentDatabaseRow']['pid']);
        }

        if ($pageId > 0) {
            $templateLayouts = $this->templateLayoutsUtility->getAvailableTemplateLayouts($pageId);
            foreach ($templateLayouts as $layout) {
                $additionalLayout = [
                    htmlspecialchars($this->getLanguageService()->sL($layout[0])),
                    $layout[1]
                ];
                array_push($config['items'], $additionalLayout);
            }
        }
    }

    /**
     * Get tt_content record
     *
     * @param int $uid
     * @return array
     */
    protected function getContentElementRow($uid)
    {
        return BackendUtilityCore::getRecord('tt_content', $uid);
    }

    /**
     * Get page id, if negative, then it is a "after record"
     *
     * @param int $pid
     * @return int
     */
    protected function getPageId($pid)
    {
        $pid = (int)$pid;

        if ($pid > 0) {
            return $pid;
        } else {
            $row = BackendUtilityCore::getRecord('tt_content', abs($pid), 'uid,pid');
            return $row['pid'];
        }
    }

    /**
     * Returns LanguageService
     *
     * @return \TYPO3\CMS\Lang\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

}
